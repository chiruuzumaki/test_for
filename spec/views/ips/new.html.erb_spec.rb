require 'rails_helper'

RSpec.describe "ips/new", type: :view do
  before(:each) do
    assign(:ip, Ip.new())
  end

  it "renders new ip form" do
    render

    assert_select "form[action=?][method=?]", ips_path, "post" do
    end
  end
end
