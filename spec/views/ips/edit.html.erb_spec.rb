require 'rails_helper'

RSpec.describe "ips/edit", type: :view do
  before(:each) do
    @ip = assign(:ip, Ip.create!())
  end

  it "renders the edit ip form" do
    render

    assert_select "form[action=?][method=?]", ip_path(@ip), "post" do
    end
  end
end
